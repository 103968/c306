public class Bid {

	private String category;
	private int year;
	private String month;
	private int round;
	private int quota;
	private int bidders;
	private double premium;

	public Bid(String category, int year, String month, int round, int quota,
			int bidders, double premium) {
		this.category = category;
		this.year = year;
		this.month = month;
		this.round = round;
		this.quota = quota;
		this.bidders = bidders;
		this.premium = premium;
	}

	public double getPremium() {
		return premium;
	}

	public int getRound() {
		return round;
	}

	public int getBidders() {
		return bidders;
	}

	public int getYear() {
		return year;
	}

	public String getMonth() {
		return month;
	}

	public int getQuota() {
		return quota;
	}

	public boolean isThisBid(int year, String month, int round) {
		boolean result = false;
		if ((this.year == year) && (this.month.equals(month))
				&& (this.round == round)) {
			result = true;
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bidders;
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		long temp;
		temp = Double.doubleToLongBits(premium);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + quota;
		result = prime * result + round;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Bid other = (Bid) obj;
		if (bidders != other.bidders) {
			return false;
		}
		if (category == null) {
			if (other.category != null) {
				return false;
			}
		} else if (!category.equals(other.category)) {
			return false;
		}
		if (month == null) {
			if (other.month != null) {
				return false;
			}
		} else if (!month.equals(other.month)) {
			return false;
		}
		if (Double.doubleToLongBits(premium) != Double
				.doubleToLongBits(other.premium)) {
			return false;
		}
		if (quota != other.quota) {
			return false;
		}
		if (round != other.round) {
			return false;
		}
		if (year != other.year) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String
				.format("Bid [category=%s, year=%s, month=%s, round=%s, quota=%s, bidders=%s, premium=%s]",
						category, year, month, round, quota, bidders, premium);
	}
}
