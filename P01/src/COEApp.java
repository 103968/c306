import java.util.ArrayList;

public class COEApp {

	public double displayAveragePremium(ArrayList<Bid> bids) {

		double total = 0;
		int count = 0;
		
		for (Bid i : bids) {
			total += i.getPremium();
			count++;
		}
		
		double avg = total/count;
		
		return avg;
	}
	
	public Bid findBidWithHighestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 4 - Calculate and return the Bid with Highest COE Premium
		
		Bid highest = bids.get(0);
		for (Bid i : bids) {
			if (i.getPremium() > highest.getPremium()) {
				highest = i;
			}
		}
			
		return highest;
	}

	public Bid findBidWithLowestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 5 - Calculate and return the Bid with Lowest COE Premium
		
		Bid lowest = bids.get(0);
		for (Bid i : bids) {
			if (i.getPremium() < lowest.getPremium()) {
				lowest = i;
			}
		}
		
		return lowest;
	}

	public double calcTotalPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 6 - Calculate and return Total COE Premium
		
		double total = 0;
		for (Bid i : bids) {
			total += i.getPremium();
		}
		
		return total;
	}

	public Bid findBid(ArrayList<Bid> bids, int year, String month, int round) {
		// TODO: P01 Task 7 - Find and return Bid for specified info
		
		Bid specificBid = null;
		for (Bid i : bids) {
			if (i.isThisBid(year, month, round)) {	
				specificBid = i;
				break;
			}
		}
		
		return specificBid;
	}
	
	public ArrayList<Bid> findGreatestDifferenceInPremium(ArrayList<Bid> bids) {
		return null;
	}

	public ArrayList<Bid> filterBidsByMonth(ArrayList<Bid> bids, String month) {
		return null;
	}
}