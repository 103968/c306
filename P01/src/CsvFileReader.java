import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class CsvFileReader {
	public ArrayList<Bid> loadBids(String filename) {
		ArrayList<Bid> result = null;
		try {
			Scanner scanner = new Scanner(new File(filename));
			result = extractBids(scanner);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public ArrayList<Bid> extractBids(Scanner scanner) {
		// TODO: P01 Task 1 - Initalise an ArrayList of Bid objects
		ArrayList<Bid> list = new ArrayList<Bid>();

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] column = line.split(",");

			// TODO: P01 Task 3 - Call createdBid() and store the result in
			// the ArrayList
			Bid bid = createBid(column);
			list.add(bid);
		}
		return list;
	}

	public Bid createBid(String[] column) {
		int year = Integer.parseInt(column[0]);
		String month = column[1];
		int round = Integer.parseInt(column[2]);
		double premium = Double.parseDouble(column[3]);
		int quota = Integer.parseInt(column[4]);
		int bids = Integer.parseInt(column[5]);

		// TODO: P01 Task 2 - Create a Bid object and return it (instead of null)
		return new Bid("A", year, month, round, quota, bids, premium);
	}
}
